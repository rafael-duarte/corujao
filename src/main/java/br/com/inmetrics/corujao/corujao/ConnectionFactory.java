package br.com.inmetrics.corujao.corujao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	final String url = "jdbc:postgresql://localhost:5432/corujao";
	final String user = "postgres";
	final String password = "postgres";

	public Connection getConnection() {
		try {
			return DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			System.out.println(e);
			throw new IllegalAccessError("Nao foi possivel efetuar a conexao");
		}
	}
}
