package br.com.inmetrics.corujao.corujao;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import utils.Validate;

public class FXCadastroControl {

	Validate val = new Validate();

	@FXML
	private Button button0;

	@FXML
	private TextField nome;

	@FXML
	private TextField cpf;

	@FXML
	private TextField endereco;

	@FXML
	private TextField idade;

	@FXML
	private TextField senha;

	@FXML
	private TextField senhaCheck;

	@FXML
	void cadastrarCliente(ActionEvent event) {

		try {

			for (int i = 0; i < nome.getText().length(); i++) {
				if (Character.isDigit(nome.getText().charAt(i)) == true) {
					JOptionPane.showConfirmDialog(null, "Nome Inválido, Insira novamente o nome", "", 0);
					nome.clear();
					break;
				}
			}

			for (int i = 0; i < cpf.getText().length(); i++) {
				if (Character.isLetter(cpf.getText().charAt(i)) == true) {
					JOptionPane.showConfirmDialog(null, "CPF Inválido, Insira-o novamente", "", 0);
					cpf.clear();
					break;
				}
			}

			if (cpf.getText().length() < 11) {
				JOptionPane.showConfirmDialog(null, "CPF Inválido, Insira-o novamente", "", 0);
				cpf.clear();
			}

			for (int i = 0; i < idade.getText().length(); i++) {
				if (Character.isLetter(idade.getText().charAt(i)) == true) {
					JOptionPane.showConfirmDialog(null, "Idade Inválido, Insira-o novamente", "", 0);
					idade.clear();
					break;
				}
			}

			if (senha.getText().length() >= 0 || senhaCheck.getText().length() >= 0) {
				if (!senha.getText().equals(senhaCheck.getText())) {
					senhaCheck.clear();
					JOptionPane.showConfirmDialog(null, "Senha Inválida não semelhante à primeira, Insira-o novamente",
							"", 0);

				}
			}

			String[] cpfLis = cpf.getText().split("");
			String usuario = nome.getText();

			// usuario será igual ao nome concatenado com os 4 ultimos numero do
			// cpf
			for (int i = 7; i < cpfLis.length; i++) {

				usuario = usuario + cpfLis[i];
			}

			if (cpf.getText().length() != 0 && nome.getText().length() != 0 && idade.getText().length() != 0
					&& senha.getText().length() != 0 && senhaCheck.getText().length() != 0
					&& endereco.getText().length() != 0) {

				Cliente cliente = new Cliente();

				cliente.setCpf(Long.parseLong(cpf.getText()));
				cliente.setNome(nome.getText());
				cliente.setEndereco(endereco.getText());
				cliente.setIdade(Integer.parseInt(idade.getText()));
				cliente.setSenha(senha.getText());

				ClienteDAO clienteDao = new ClienteDAO();
				clienteDao.inserir(cliente, usuario);

				int i = JOptionPane.showConfirmDialog(null, "Cliente cadastrado no banco! Deseja fazer novo cadastro?",
						"Usuário cadastrado no banco!", 0);

				endereco.clear();
				idade.clear();
				cpf.clear();
				nome.clear();
				senha.clear();
				senhaCheck.clear();

			} else {
				JOptionPane.showConfirmDialog(null, "Dados não cadastrados, Insira-o novamente", "", 0);
			}

		} catch (Exception e) {

		}

	}

	@FXML
	void checkCPF(ActionEvent event) {
		val.validacaodeLong(cpf.getText());
	}

	@FXML
	void checkIdade(ActionEvent event) {
		val.validacaodeLong(idade.getText());
	}

}
