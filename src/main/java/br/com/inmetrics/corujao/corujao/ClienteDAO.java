package br.com.inmetrics.corujao.corujao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class ClienteDAO {

	private Connection connection;

	public ClienteDAO() {
		connection = new ConnectionFactory().getConnection();
	}

	public boolean logar(String usuario, String senha) {

		try {
			PreparedStatement stmt = connection.prepareStatement("select * from coruja where usuario = ? and senha=?");
			stmt.setString(1, usuario);
			stmt.setString(2, senha);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}

	public void inserir(Cliente obj, String user) {
		try {
			PreparedStatement stmt = connection.prepareStatement(
					"INSERT INTO public.coruja(id, name, idade, cpf, endereco, senha, usuario)VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, obj.getNome());
			stmt.setInt(2, obj.getIdade());
			stmt.setLong(3, obj.getCpf());
			stmt.setString(4, obj.getEndereco());
			stmt.setString(5, obj.getSenha());
			stmt.setString(6, user);
			stmt.execute();

			int i = JOptionPane.showConfirmDialog(null, "Cliente cadastrado no banco! Deseja fazer novo cadastro?",
					"Usuário cadastrado no banco!", 0);

		} catch (SQLException e) {
			int i = JOptionPane.showConfirmDialog(null, "erro, ao incluir o registro na base",
					"Usuário cadastrado no banco!", 0);
			throw new IllegalAccessError("erro, ao incluir o registro na base");
		}
	}

	public void deletar(int id) {
		try {
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM public.coruja	WHERE id=?");
			stmt.setInt(1, id);
			stmt.execute();
		} catch (SQLException e) {
			throw new IllegalAccessError("erro, ao deletar o registro na base");
		}
	}

	public void alterar(Cliente obj) {
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"O que deseja alterar neste cliente? Digite a letra correspondente /n-I para alterar idade /n-E para alterar endere�o ");
		String respostaAlterar = sc.next();

		try {
			PreparedStatement stmt = connection.prepareStatement("update coruja set name = ? where id = ?");
			stmt.setString(1, obj.getNome());
			stmt.setString(2, String.valueOf(obj.getId()));
			stmt.execute();
		} catch (SQLException e) {
			throw new IllegalAccessError("erro, ao alterar o registro na base");
		}
	}

	public void fecharConexao() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Cliente consultarPorNome(String text) {

		Cliente clienteConsultado = new Cliente();
		int id = 0;

		try {
			PreparedStatement stmt = connection.prepareStatement("select * from public.coruja where name = ?");
			stmt.setString(1, text);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				clienteConsultado.setNome(rs.getString("name"));
				clienteConsultado.setCpf(Long.parseLong(rs.getString("cpf")));
				id = Integer.parseInt(rs.getString("id"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		clienteConsultado.setId(id);

		return clienteConsultado;

	}

	public void contador(int ID) {

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma
														// que tem
		String dataFormatada = sdf.format(hora);
		try {
			PreparedStatement stmt = connection.prepareStatement("UPDATE public.coruja set inicio_hora=? where id = ?");
			stmt.setString(1, dataFormatada);
			stmt.setInt(2, ID);
			stmt.executeQuery();
			stmt.close();

		} catch (SQLException e) {

		}

	}

	public void encerraContador(int ID) {

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date horaFinal = Calendar.getInstance().getTime();
		String horaEncerrada = sdf.format(horaFinal);

		String[] horaInicialList = new String[2];
		String[] horaFinalList = new String[2];

		try {
			PreparedStatement stmt = connection.prepareStatement("select * from coruja where id = ?");
			stmt.setInt(1, ID);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				horaInicialList = rs.getString("inicio_hora").split(":");
			}

			double h = Double.parseDouble(horaInicialList[0]);
			double h2 = Double.parseDouble(horaInicialList[1]);
			double hInicial = h * 60 + h2;

			horaFinalList = horaEncerrada.split(":");

			double hF = Double.parseDouble(horaFinalList[0]);
			double h2F = Double.parseDouble(horaFinalList[1]);
			double hFinal = hF * 60 + h2F;

			double horarioTotal = (hFinal - hInicial) / 60;
			if (horarioTotal < 1) {
				horarioTotal = 1;
			}
			JOptionPane.showMessageDialog(null,
					"O total de horas a receber do cliente é de " + horarioTotal + " horas");

		} catch (SQLException e) {

		}

	}

	public ArrayList<Cliente> buscarTodosOsClientes() {

		ArrayList<Cliente> listaDeClientes = new ArrayList<Cliente>();
		try {
			PreparedStatement stmt = connection.prepareStatement("select * from coruja");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Cliente clienteParaLista = new Cliente();
				clienteParaLista.setId(rs.getInt("id"));
				clienteParaLista.setNome(rs.getString("name"));
				clienteParaLista.setIdade(rs.getInt("idade"));
				clienteParaLista.setCpf(rs.getLong("cpf"));
				clienteParaLista.setEndereco(rs.getString("endereco"));

				listaDeClientes.add(clienteParaLista);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listaDeClientes;
	}

}
