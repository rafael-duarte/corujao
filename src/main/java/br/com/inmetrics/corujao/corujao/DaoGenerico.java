package br.com.inmetrics.corujao.corujao;


import java.util.List;

public interface DaoGenerico<T> {
	
	
	public void inserir(T obj);
	
	public void deletar(int id);
	
	public void alterar(T obj);

	public List<T> buscarTodosOsClientes();

}
