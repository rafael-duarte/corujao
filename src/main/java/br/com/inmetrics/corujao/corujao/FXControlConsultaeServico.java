package br.com.inmetrics.corujao.corujao;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXControlConsultaeServico {

	ClienteDAO clienteConsulta = new ClienteDAO();
	Cliente clienteConsultado = new Cliente();

	@FXML
	private TextField dadodeBusca;

	@FXML
	private Label perguntaIdouNome;

	@FXML
	private Button btn;

	@FXML
	private Label nomeCliente;

	@FXML
	private Button encerrarContador;

	@FXML
	void carregaOpcaoSelecionada(ActionEvent event) {

		nomeCliente.setText(clienteConsulta.consultarPorNome(dadodeBusca.getText()).getNome() + " "
				+ clienteConsulta.consultarPorNome(dadodeBusca.getText()).getCpf());

		if (nomeCliente.getText().contains("null")) {

			JOptionPane.showMessageDialog(null, "Usuário não encontrado, digite novamente.");

		} else {

			int i = JOptionPane.showConfirmDialog(null, "Deseja iniciar contador para "
					+ clienteConsulta.consultarPorNome(dadodeBusca.getText()).getNome() + "?", "", 0);

			clienteConsultado = clienteConsulta.consultarPorNome(dadodeBusca.getText());
			if (i == 0) {
				clienteConsulta.contador(clienteConsultado.getId());
			}
		}
	}

	@FXML
	void encerraContador(ActionEvent event) {
		clienteConsulta.encerraContador(clienteConsultado.getId());
	}

}
