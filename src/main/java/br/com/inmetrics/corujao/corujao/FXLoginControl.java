package br.com.inmetrics.corujao.corujao;

import java.io.IOException;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FXLoginControl {

	@FXML
	private Button loginButton;

	@FXML
	private TextField user;

	@FXML
	private TextField password;

	@FXML
	public void toLog(ActionEvent event) {

		ClienteDAO cLogin = new ClienteDAO();
		boolean logar = cLogin.logar(user.getText(), password.getText());

		if (user.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "O usuario não pode ser vazio");
		}

		else if (password.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "A senha não pode ser vazio");
		} else {

			if (logar) {

				try {

					loginButton.getScene().getWindow().hide();
					Pane root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
					Scene scene = new Scene(root);
					Stage stage = new Stage();
					stage.setScene(scene);
					stage.show();

				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				JOptionPane.showMessageDialog(null, "Usuário ou senha inválidos! Tente novamente!");
				user.clear();
				password.clear();
			}

		}
	}
}
