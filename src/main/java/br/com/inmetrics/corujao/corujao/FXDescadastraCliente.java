package br.com.inmetrics.corujao.corujao;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXDescadastraCliente {

	ClienteDAO clienteConsulta = new ClienteDAO();
	Cliente clienteConsultado = new Cliente();

	@FXML
	private TextField nomeDescadastro;

	@FXML
	private Button btn;

	@FXML
	private Label nomeCliente;

	@FXML
	void carregaOpcaoSelecionada(ActionEvent event) {
		nomeCliente.setText(clienteConsulta.consultarPorNome(nomeDescadastro.getText()).getNome() + " "
				+ clienteConsulta.consultarPorNome(nomeDescadastro.getText()).getCpf());

		if (nomeCliente.getText().contains("null")) {

			JOptionPane.showMessageDialog(null, "Usuário não encontrado, digite novamente.");

		} else {

			int i = JOptionPane
					.showConfirmDialog(null,
							"Deseja descadastrar "
									+ clienteConsulta.consultarPorNome(nomeDescadastro.getText()).getNome() + "?",
							"", 0);

			clienteConsultado = clienteConsulta.consultarPorNome(nomeDescadastro.getText());
			if (i == 0) {
				clienteConsulta.deletar(clienteConsultado.getId());
				JOptionPane.showMessageDialog(null, "Usuário " + nomeDescadastro.getText() + " descadastrado.");
				nomeCliente.setText("");
			}
		}
	}

	@FXML
	void descadastrar(ActionEvent event) {

	}

}
