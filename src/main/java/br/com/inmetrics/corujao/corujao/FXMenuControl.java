package br.com.inmetrics.corujao.corujao;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FXMenuControl implements Initializable {

	@FXML
	private MenuBar menuBar;

	@FXML
	private MenuItem chamaCadastro;

	@FXML
	private MenuItem chamaConsulta;

	@FXML
	void callCadastro(ActionEvent event) {
		constroiTela("Cadastro.fxml", menuBar.getScene());
	}

	@FXML
	void callConsulta(ActionEvent event) {
		constroiTela("ConsultaCliente.fxml", menuBar.getScene());
	}

	@FXML
	void callDescadastramento(ActionEvent event) {
		constroiTela("DescadastraCliente.fxml", menuBar.getScene());
	}

	public void constroiTela(String local, Scene cena) {
		Pane root;
		try {
			root = FXMLLoader.load(getClass().getResource(local));
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void initialize(URL location, ResourceBundle resources) {

	}
}
