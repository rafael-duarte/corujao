package utils;

import javax.swing.JOptionPane;

public class Validate {

	public boolean validacaodeInt(String possivelInt) {

		try {
			int numero = Integer.parseInt(possivelInt);
			return true;
		} catch (Exception e) {
			System.out.println(e);
		}
		JOptionPane.showConfirmDialog(null, "Idade inválida", "", 0);
		return false;
	}

	public boolean validacaodeLong(String possivelLong) {

		try {
			Long numero = Long.parseLong(possivelLong);
			return true;
		} catch (Exception e) {
			System.out.println(e);
		}
		JOptionPane.showConfirmDialog(null, "CPF inválido", "", 0);
		return false;
	}

	public boolean validacaodeString(String possivelNumero) {

		try {
			int numero = Integer.parseInt(possivelNumero);
			JOptionPane.showConfirmDialog(null, "Nome inválido", "", 0);
			return false;
		} catch (Exception e) {
			System.out.println(e);
			return true;
		}
	}

}
