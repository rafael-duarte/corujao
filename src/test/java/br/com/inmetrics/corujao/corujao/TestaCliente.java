package br.com.inmetrics.corujao.corujao;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class TestaCliente {

	@Test
	public void test() {
		Set<Cliente> cliente = new HashSet<Cliente>();
		cliente.add(new Cliente("Laura", 12, 40490897851L, "Rua dos Tolos", "", ""));
		cliente.add(new Cliente("Lau", 21, 40490897851L, "Rua dos Tolos", "", ""));
		cliente.add(new Cliente("L", 29, 40490897851L, "Rua dos Tolos", "", ""));
		cliente.add(new Cliente("Lura", 50, 40490897851L, "Rua dos Tolos", "", ""));

		cliente.forEach(
				Cliente -> System.out.println(((Cadastro) cliente).getNome() + " - " + ((Cadastro) cliente).getId()));

	}

}
