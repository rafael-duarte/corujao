package br.com.inmetrics.corujao.corujao;

import java.util.List;

import org.junit.Test;

import junit.framework.TestCase;

public class TestConnection extends TestCase {

	@Test
	public void TestConnection() {

		ClienteDAO clienteDAO = new ClienteDAO();

		clienteDAO.inserir(new Cliente("Laura", 12, 40490897851L, "Rua dos Tolos", "", ""), "Laura7851");
		clienteDAO.inserir(new Cliente("Jeremias", 12, 40490897851L, "Rua dos Tolos", "", ""), "Jeremias7851");
		clienteDAO.inserir(new Cliente("Fineias", 12, 40490897851L, "Rua dos Tolos", "", ""), "Fineias7851");
		clienteDAO.inserir(new Cliente("Ruben", 12, 40490897851L, "Rua dos Tolos", "", ""), "Ruben7851");

		// Listar todas as clientes
		List<Cliente> clientesAntesDaEdicao = clienteDAO.buscarTodosOsClientes();
		clientesAntesDaEdicao.forEach(cliente -> System.out.println(cliente.getNome()));

		// Alterar registro na posicao zero
		Cliente clienteParaAlterar = clientesAntesDaEdicao.get(0);
		clienteParaAlterar.setNome("Ageu");
		clienteDAO.alterar(clienteParaAlterar);

		// Deletar registro
		Cliente clienteParaDeletar = clientesAntesDaEdicao.get(0);
		clienteDAO.deletar(clienteParaDeletar.getId());

		List<Cliente> clientesDepoisDeAlterar = clienteDAO.buscarTodosOsClientes();
		clientesDepoisDeAlterar.forEach(cliente -> System.out.println(cliente.getNome()));

		clienteDAO.fecharConexao();
	}

}
